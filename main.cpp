#include <cstdlib>
#include <time.h>
#include "grafo.cpp" 

int main() {
    srand(time(NULL));
    int tam = 50;
    int marcados[] = {3, 4, 7, 9};
    GrafoTipo<int> glucogeno(tam);
    //se crean los vertices
    for (int i=0; i < tam; i++){
        glucogeno.AgregarVertice(i);
    }

    //se crea la rama principal del glucogeno
    for (int i=0; i < 10; i++){
        glucogeno.AgregarArista(i, i+1, 33);
        // marcas en 3, 7, 9, 4
    }
    //se agregan aristas en los puntos siguientes:
    glucogeno.AgregarArista(4, 10, 32);
    glucogeno.AgregarArista(3, 11, 32);
    glucogeno.AgregarArista(7, 12, 32);
    glucogeno.AgregarArista(9, 13, 32);



    //algoritmo para añadir vertices a las aristas creadas
    //asi se crea la forma "ramificada" del glucogeno
    int actual=10;
    int conectar = 14;
    while (actual < 14){
        //la primera de cada arista pesa 32, por eso se hace fuera del while
        glucogeno.AgregarArista(actual, conectar, 32);
        for (int i=0; i < rand()%10; i++){
            glucogeno.AgregarArista(conectar, conectar+1, 33);
            conectar++;
        }
        actual++;
    }
    //si es que quedan nodos sobrantes se agregan al final de la cadena principal
    while (!glucogeno.EstaLleno()){
        glucogeno.AgregarArista(9, conectar, 33);
        conectar++;
    }
    //se prueba el algoritmo de dijkstra 
    glucogeno.dijkstra(9);
  
    return 0;
}
