const int NULL_EDGE = 0;
const int INFINITO = 999999;
template<class T>
class GrafoTipo {
  public:
    GrafoTipo(int);
    ~GrafoTipo();
    void AgregarVertice(T);
    void AgregarArista(T, T, int);
    int Peso(T, T);
    int Indice(T*, T);
    bool EstaVacio();
    bool EstaLleno(); 
    int Tamanio();
    int DistanciaMinima();
    void ImprimirSolucion();
    void dijkstra(T);

 private:
    int numVertices;
    int maxVertices;

    int* vertices;
    int **aristas;


    bool* marks;
    int *dist;
}; 