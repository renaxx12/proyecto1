#include "grafo.h"
#include <iostream>

using namespace std;

template<class T>
GrafoTipo<T>::GrafoTipo(int maxV){
   numVertices = 0;
   maxVertices = maxV;
   vertices = new int[maxV];
   aristas = new int*[maxV];
 
   for(int i = 0; i < maxV; i++)
     aristas[i] = new int[maxV];
 
   marks = new bool[maxV];
   dist = new int[maxV];
}
template<class T>
GrafoTipo<T>::~GrafoTipo(){
   delete [] vertices;
 
   for(int i = 0; i < maxVertices; i++)
      delete [] aristas[i];
 
   delete [] aristas;
   delete [] marks;
} 
template<class T>
void GrafoTipo<T>::AgregarVertice(T vertice){
   vertices[numVertices] = vertice;

   for(int indice = 0; indice < numVertices; indice++) {
     aristas[numVertices][indice] = NULL_EDGE;
     aristas[indice][numVertices] = NULL_EDGE;
   }
   
   numVertices++;
}
template<class T>
void GrafoTipo<T>::AgregarArista(T desdeVertice, T haciaVertice, int peso){
   int fila, col;

   fila = Indice(vertices, desdeVertice);
   col = Indice(vertices, haciaVertice);
   aristas[fila][col] = peso;
} 
template<class T>
int GrafoTipo<T>::Peso(T desdeVertice, T haciaVertice){
   int fila, col;

   fila = Indice(vertices, desdeVertice);
   col = Indice(vertices, haciaVertice);
   
   return aristas[fila][col];
} 
template<class T>
int GrafoTipo<T>::Indice(T *vertices, T vertice){
  for(int i = 0; i < numVertices; i++){
      if (vertices[i] == vertice)
          return i;
  }

  return -1;
}
template<class T>
bool GrafoTipo<T>::EstaLleno(){
  if (numVertices == maxVertices)
      return true;
  
  return false;
}
template<class T>
bool GrafoTipo<T>::EstaVacio(){
  if (numVertices == 0)
      return true;
  
  return false;
}
template<class T>
int GrafoTipo<T>::Tamanio(){
  return numVertices;
}


// A utility function to find the vertex with minimum
// distance value, from the set of vertices not yet included
// in shortest path tree
template<class T>
int GrafoTipo<T>::DistanciaMinima()
{
	// Initialize min value
	int minimo = INFINITO, min_index;

	for (int i = 0; i < maxVertices; i++)
		if (marks[i] == false && dist[i] <= minimo)
			minimo = dist[i], min_index = i;

	return min_index;
}

// A utility function to print the constructed distance
// array
template<class T>
void GrafoTipo<T>::ImprimirSolucion()
{
	cout << "Vertice \t distancia del origen" << endl;
	for (int i = 0; i < maxVertices; i++)
		cout << i << " \t\t\t\t" << dist[i] << endl;
}

// Function that implements Dijkstra's single source
// shortest path algorithm for a aristas represented using
// adjacency matrix representation
template<class T>
void GrafoTipo<T>::dijkstra(T vertice){
	
  int src = Indice(vertices, vertice);
	for (int i = 0; i < maxVertices; i++)
		dist[i] = INFINITO, marks[i] = false;

	// Distance of source vertex from itself is always 0
	dist[src] = 0;

	// Find shortest path for all vertices
	for (int contador = 0; contador < maxVertices - 1; contador++) {
		// Pick the minimum distance vertex from the set of
		// vertices not yet processed. aux is always equal to
		// src in the first iteration.
		int aux = DistanciaMinima();

		// Mark the picked vertex as processed
		marks[aux] = true;

		// Update dist value of the adjacent vertices of the
		// picked vertex.
		for (int i = 0; i < maxVertices; i++)

			if (!marks[i] && aristas[aux][i]
				&& dist[aux] != INFINITO
				&& dist[aux] + aristas[aux][i] < dist[i])
				dist[i] = dist[aux] + aristas[aux][i];
	}

	// print the constructed distance array
	ImprimirSolucion();
}

